import os
import pickle
from sklearn.cluster import KMeans, DBSCAN

from constants import DOC_VECS_FORMAT


def main():
    text_input = input('Please input your related doc text name : ')

    if not os.path.exists(DOC_VECS_FORMAT.format(text_input)):
        print('Stopped - The doc vecs are not available...')
        return

    print('Loading doc vecs...')
    doc_vecs = pickle.load(open(DOC_VECS_FORMAT.format(text_input), 'rb'))

    print('Creating clusters...')
    vecs = [
        doc['vec'] for doc in doc_vecs
    ]
    model = KMeans(n_clusters=8).fit(vecs)

    print('Predicting...')
    for doc_vec in doc_vecs:
        doc_vec['cluster'] = model.predict([doc_vec['vec']])[0]

    for x in range(0, 10):
        with open('data/{}-cluster-{}.txt'.format(text_input, x), 'w') as f:
            for doc_vec in doc_vecs:
                if doc_vec['cluster'] == x:
                    f.write(doc_vec['doc'])
                    f.write('\n')

main()