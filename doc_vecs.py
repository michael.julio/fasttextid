import fastText
import pickle

from constants import DOCS_FORMAT, DOC_VECS_FORMAT, MODEL_PATH


def main():
    model = fastText.load_model(MODEL_PATH)

    text_input = input('Please input your related doc text name : ')

    print('Creating new doc vecs for {}...'.format(text_input))
    with open(DOCS_FORMAT.format(text_input), 'r') as f:
        docs = f.read().splitlines()

    pickle.dump([{
        'doc': doc,
        'vec': model.get_sentence_vector(doc)
    } for doc in docs], open(DOC_VECS_FORMAT.format(text_input), 'wb'))


main()
