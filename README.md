# fasttextid

### Getting Started
1. Install fasttext based on https://github.com/facebookresearch/fastText/tree/master/python
2. Download pre-trained Indonesia model from https://github.com/facebookresearch/fastText/blob/master/pretrained-vectors.md
3. Extract the model to directory *model*
4. Rename `MODEL_PATH` at `constants.py` to the extracted model path

### Generating documents' vector
1. Put the document in `.txt` format inside *data* folder ( if the folder is not available, create one )
2. Run `python doc_vecs.py`
3. Input the document file name without the extension, e.g. the document file name is `document.txt` then your input should be `document`
4. Once it's finished, the result will be `{input}-vecs.pickle` inside the *data* folder

### Predict similar documents
1. Put the document in `.txt` format inside *data* folder ( if the folder is not available, create one )
2. Before predicting document, you must have document vectors file that will be used for predicting in format `{name}-vecs.pickle` inside the *data* folder
3. Run `python predict_doc.py`
4. Input the document vectors name, e.g. the document vectors file name is `document-vecs.pickle` then your input should be `document`
5. Input the document file name that will be predicted without the extension, e.g. the document file name is `document.txt` then your input should be `document`
6. Once it's finished, the result will be `{document_name}-predict.txt` inside the *data* folder