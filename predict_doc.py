import pickle
import os
from scipy import spatial

import fastText

from constants import DOC_VECS_FORMAT, MODEL_PATH


def main():
    model = fastText.load_model(MODEL_PATH)

    text_input = input('Please input your related doc text name : ')

    if not os.path.exists(DOC_VECS_FORMAT.format(text_input)):
        print('Stopped - The doc vecs are not available...')
        return

    print('Loading doc vecs...')
    doc_vecs = pickle.load(open(DOC_VECS_FORMAT.format(text_input), 'rb'))

    doc_input = input('Please input the doc name to be predicted : ')
    with open('data/{}.txt'.format(doc_input), 'r') as f:
        docs = f.read().splitlines()

    doc = docs[0]
    vector = model.get_sentence_vector(doc)

    max_score = 0
    min_score = 1
    with open('data/{}-predict.txt'.format(doc_input), 'w') as f:
        filtered_list = []
        for doc_vec in doc_vecs:
            score = 1 - spatial.distance.cosine(vector, doc_vec['vec'])
            doc_vec['score'] = score
            if score > max_score:
                max_score = score
            if score < min_score:
                min_score = score

            if score > 0.97:
                filtered_list.append(doc_vec)

        sorted_list = sorted(filtered_list, key=lambda k:k['score'], reverse=True)
        for doc_vec in sorted_list:
            f.write(doc_vec['doc'])
            f.write('\n')
            f.write(str(doc_vec['score']))
            f.write('\n')
            f.write('\n')

    print('Max Score : {}'.format(max_score))
    print('Min Score : {}'.format(min_score))


main()
